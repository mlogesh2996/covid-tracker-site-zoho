import React, { useReducer, useState } from "react";
import Header from "../../components/Header/Header";
import FilterContainer from "../../components/FilterContainer/FilterContainer";
import Dashboard from "../../components/Dashboard/Dashboard";
import useFetch from "../../components/hooks/useFetchAPI";

export default function Home() {
  const response = useFetch(
    `https://data.covid19india.org/v4/min/data.min.json`,
    {}
  );
  const timeSeriesResponse = useFetch(
    `https://data.covid19india.org/v4/min/timeseries.min.json`,
    {}
  );

  //Lifting states up so every child components can access it
  const [currentFilterType, setCurrentFilterType] = useState();
  const [filterValues, setFilterValues] = useReducer(
    (state: any, newState: any) => ({ ...state, ...newState }),
    {
      search: "",
      date: "",
      sort: "",
    }
  );

  return (
    <>
      <Header />
      <div
        style={{
          background: "#f1f1f1",
          minHeight: "1200px",
          paddingTop: "30px",
          paddingBottom: "45px",
          minWidth: "992px",
        }}
      >
        <div>
          <FilterContainer
            currentFilterType={currentFilterType}
            filterValues={filterValues}
            setFilterValues={setFilterValues}
            setCurrentFilterType={setCurrentFilterType}
          />
        </div>
        <div
          style={{ borderTop: "2px solid #bfbfbf" }}
          className="mt-3 ml-5 mr-5"
        ></div>
        <Dashboard
          currentFilterType={currentFilterType}
          filterValues={filterValues}
          response={response}
          timeSeriesResponse={timeSeriesResponse}
        />
      </div>
    </>
  );
}
