import { useRouter } from "next/router";
import Header from "../../components/Header/Header";
import FilterContainer from "../../components/FilterContainer/FilterContainer";
import SimpleTable from "../../components/SimpleTable/SimpleTable";
import useFetch from "../../components/hooks/useFetchAPI";
import { getSortedResultforTable } from "../../utils/common";
import { useReducer, useState } from "react";

export default function DetailPage() {
  const router = useRouter();
  const stateCode: any = router?.query?.id || "AN";

  const timeSeriesResponse = useFetch(
    `https://data.covid19india.org/v4/min/timeseries.min.json`,
    {}
  );

  //Lifting states up so every child components can access it
  const [currentFilterType, setCurrentFilterType] = useState();
  const [filterValues, setFilterValues] = useReducer(
    (state: any, newState: any) => ({ ...state, ...newState }),
    {
      search: "",
      date: "",
      sort: "",
    }
  );

  var data: any = [];

  if (
    timeSeriesResponse?.response &&
    Object.keys(timeSeriesResponse.response).length > 0
  ) {
    Object.keys(timeSeriesResponse.response[stateCode].dates).forEach(
      (date) => {
        if (
          (currentFilterType === "date" && filterValues.date == date) ||
          !currentFilterType ||
          currentFilterType === "sort"
        ) {
          //Delta
          let deltaConfirmed =
            timeSeriesResponse.response[stateCode].dates[date]?.delta
              ?.confirmed || 0;
          let deltaRecovered =
            timeSeriesResponse.response[stateCode].dates[date]?.delta
              ?.recovered || 0;
          let deltaDeceased =
            timeSeriesResponse.response[stateCode].dates[date]?.delta
              ?.deceased || 0;
          //Delta7
          let delta7Confirmed =
            timeSeriesResponse.response[stateCode].dates[date]?.delta7
              ?.confirmed || 0;
          let delta7Recovered =
            timeSeriesResponse.response[stateCode].dates[date]?.delta7
              ?.recovered || 0;
          let delta7Deceased =
            timeSeriesResponse.response[stateCode].dates[date]?.delta7
              ?.deceased || 0;

          data.push({
            date: date,
            confirmed:
              timeSeriesResponse.response[stateCode].dates[date]?.total
                ?.confirmed || 0,
            recovered:
              timeSeriesResponse.response[stateCode].dates[date]?.total
                ?.recovered || 0,
            deceased:
              timeSeriesResponse.response[stateCode].dates[date]?.total
                ?.deceased || 0,
            delta:
              "Confirmed: " +
              deltaConfirmed +
              "\nRecovered: " +
              deltaRecovered +
              "\nDeceased: " +
              deltaDeceased,
            delta7:
              "Confirmed: " +
              delta7Confirmed +
              "\nRecovered: " +
              delta7Recovered +
              "\nDeceased: " +
              delta7Deceased,
          });
        }
      }
    );
  }

  if (currentFilterType === "sort" && data.length > 0) {
    data = getSortedResultforTable(filterValues.sort, data);
  }

  const columns = [
    {
      Header: "Date",
      accessor: "date",
    },
    {
      Header: "Confirmed",
      accessor: "confirmed",
    },
    {
      Header: "Recovered",
      accessor: "recovered",
    },
    {
      Header: "Deceased",
      accessor: "deceased",
    },
    {
      Header: "Delta",
      accessor: "delta",
    },
    {
      Header: "Delta 7",
      accessor: "delta7",
    },
  ];

  return (
    <>
      <Header />
      <div
        style={{
          background: "#f1f1f1",
          minHeight: "1200px",
          paddingTop: "30px",
          paddingBottom: "45px",
          minWidth: "1635px",
        }}
      >
        <div>
          <FilterContainer
            currentFilterType={currentFilterType}
            filterValues={filterValues}
            setFilterValues={setFilterValues}
            setCurrentFilterType={setCurrentFilterType}
          />
        </div>
        <div
          style={{ borderTop: "2px solid #bfbfbf" }}
          className="mt-3 ml-5 mr-5"
        ></div>
        <p
          style={{
            fontSize: "1.5rem",
            fontWeight: 500,
            marginLeft: "3rem",
            marginTop: "2rem",
          }}
        >
          Selected State: {stateCode}
        </p>
        <SimpleTable data={data} columns={columns} />
      </div>
    </>
  );
}
