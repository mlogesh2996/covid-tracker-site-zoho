import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInputGroup } from "mdbreact";
import style from "./filtercontainer.module.scss";
import { IFilterContainerProps } from "./typings";
import { useRouter } from "next/router";
function FilterContainer(props: IFilterContainerProps) {
  const router = useRouter();
  const {
    filterValues = "",
    setFilterValues = () => {},
    setCurrentFilterType = () => {},
  } = props;

  const handleFilterChange = (e: any) => {
    e.preventDefault();
    setCurrentFilterType(e.target.name);
    setFilterValues({ [e.target.name]: e.target.value });
    //clear other filter fields
    Object.keys(filterValues).forEach((item) => {
      if (item !== e.target.name) {
        filterValues[item] = "";
      }
    });
  };

  const handleClear = (e: any) => {
    e.preventDefault();
    setFilterValues({
      search: "",
      date: "",
      sort: "",
    });
    setCurrentFilterType("");
  };

  return (
    <MDBContainer
      style={{ paddingLeft: "3rem", paddingRight: "3rem", minWidth: "800px" }}
      className={`w-100 mw-100 ${style.containercustomsm}`}
    >
      <MDBRow style={{ textAlign: "center" }} className="w-100">
        <MDBCol size="3">
          {!router.pathname.includes("detail") && (
            <form className="form-inline mt-2 mb-2">
              <span style={{ fontWeight: 500 }}>States </span>
              <input
                name="search"
                style={{ minHeight: "40px" }}
                className="form-control form-control-sm ml-3 w-75"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={filterValues.search}
                onChange={handleFilterChange}
              />
            </form>
          )}
        </MDBCol>

        <MDBCol size="3" style={{ fontWeight: 500, alignSelf: "center" }}>
          Date:{" "}
          <input
            name="date"
            type="date"
            onChange={handleFilterChange}
            value={filterValues.date}
          />
        </MDBCol>
        <MDBCol size="3" style={{ alignSelf: "center" }}>
          {" "}
          {router.pathname.includes("detail") ? (
            <MDBInputGroup
              prepend="Sort by"
              inputs={
                <select
                  name="sort"
                  className="browser-default custom-select"
                  onChange={handleFilterChange}
                  value={filterValues.sort}
                >
                  <option value="0">Choose...</option>
                  <option value="1">Confirmed (ASC)</option>
                  <option value="2">Confirmed (DESC)</option>
                  <option value="3">Recovered (ASC)</option>
                  <option value="4">Recovered (DESC)</option>
                  <option value="5">Deceased (ASC)</option>
                  <option value="6">Deceased (DESC)</option>
                </select>
              }
            />
          ) : (
            <MDBInputGroup
              prepend="Sort by"
              inputs={
                <select
                  name="sort"
                  className="browser-default custom-select"
                  onChange={handleFilterChange}
                  value={filterValues.sort}
                >
                  <option value="0">Choose...</option>
                  <option value="1">Confirmed (ASC)</option>
                  <option value="2">Affected Percentage (ASC)</option>
                  <option value="3">Vaccinated Percentage (ASC)</option>
                  <option value="4">Confirmed (DESC)</option>
                  <option value="5">Affected Percentage (DESC)</option>
                  <option value="6">Vaccinated Percentage (DESC)</option>
                </select>
              }
            />
          )}
        </MDBCol>
        <MDBCol size="3" style={{ fontWeight: 500, alignSelf: "center" }}>
          <a onClick={handleClear}>Clear applied filter</a>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}
export { FilterContainer };
export default FilterContainer;
