export interface IFilterContainerProps {
  currentFilterType?: string;
  filterValues?: any;
  setFilterValues: (data: any) => void;
  setCurrentFilterType: (data: any) => void;
}
