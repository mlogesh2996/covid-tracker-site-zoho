import React from "react";

const useFetch = (url: any, options: any) => {
  const [response, setResponse]: any = React.useState(null);
  const [error, setError]: any = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);

  React.useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const res = await fetch(url, options);
        const json = await res.json();

        setResponse(json);
        setIsLoading(false);
      } catch (error) {
        setError(error);
      }
    };

    fetchData();
  }, []);

  return { response, error, isLoading };
};

export default useFetch;
