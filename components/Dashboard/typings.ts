export interface IFilterContainerProps {
  currentFilterType?: string;
  filterValues?: any;
  response?: any;
  timeSeriesResponse?: any;
  setFilterValues?: (data: any) => void;
  setCurrentFilterType?: (data: any) => void;
}
