import React, { useState, useEffect } from "react";
import style from "./dashboard.module.scss";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import { getSearchResult, getSortedResult } from "../../utils/common";
import { CardContent } from "../CardContent/CardContent";
import { IFilterContainerProps } from "./typings";

function Dashboard(props: IFilterContainerProps) {
  const {
    currentFilterType = "",
    filterValues = "",
    response = {},
    timeSeriesResponse = {},
  } = props;

  const [stateData, setStateData] = useState([]);
  const [filteredStateCode, setFilteredStateCode]: any = useState([]);
  const [sortedStateCode, setSortedStateCode] = useState([]);
  const [isFilterHasNoResult, setIsFilterHasNoResult] = useState(false);

  //whenever currentFilterType changes this useEffect will get called
  useEffect(() => {
    if (response?.response && !stateData) {
      setStateData(response.response);
    }
  }, [response?.response]);

  //whenever currentFilterType changes this useEffect will get called
  useEffect(() => {
    if (currentFilterType === "search" && filterValues?.search) {
      let resultSet = getSearchResult(filterValues.search, response.response);

      if (resultSet?.length > 0) {
        setIsFilterHasNoResult(false);
        setFilteredStateCode(resultSet);
      } else {
        setIsFilterHasNoResult(true);
      }
    } else if (currentFilterType === "sort" && filterValues?.sort) {
      let result = getSortedResult(filterValues.sort, response.response);
      setSortedStateCode(result);
    } else if (currentFilterType === "date" && filterValues?.date) {
      let temp: any = [];
      Object.keys(timeSeriesResponse.response).forEach((val: any) => {
        let tt = timeSeriesResponse.response[val]["dates"]?.[filterValues.date];
        if (tt) {
          temp[val] = tt;
        }
      });
      setStateData(temp);
    } else {
      setIsFilterHasNoResult(false);
      setFilteredStateCode([]);
      setStateData(response.response);
    }
  }, [
    currentFilterType,
    filterValues.search,
    filterValues.sort,
    filterValues.date,
  ]);

  const renderCard = (stateCode: any, indx: number) => {
    return (
      <MDBCol
        className={`${style.colextrasm} m-3`}
        key={indx}
        xs="4"
        sm="4"
        md="4"
        lg="4"
        xl="3"
        middle
      >
        <CardContent stateData={stateData} stateCode={stateCode} />
      </MDBCol>
    );
  };

  return (
    <>
      <MDBContainer
        style={{ minWidth: "992px" }}
        className={`${style.containercustomsm}`}
      >
        <h1
          style={{ fontWeight: 400, textAlign: "center", paddingTop: "2rem" }}
        >
          Covid Stats
        </h1>
        <MDBRow>
          {currentFilterType === "sort" && sortedStateCode?.length > 0 ? (
            sortedStateCode.map((stateCode: any, indx: number) => {
              return renderCard(stateCode, indx);
            })
          ) : (
            <></>
          )}
          {stateData &&
          Object.keys(stateData).length > 0 &&
          !isFilterHasNoResult ? (
            Object.keys(stateData).map((stateCode: any, indx: number) => {
              return filteredStateCode.length > 0
                ? filteredStateCode.includes(stateCode) &&
                    renderCard(stateCode, indx)
                : renderCard(stateCode, indx);
            })
          ) : (
            <MDBCol
              style={{ textAlign: "center" }}
              className={`mt-5 ${style.colextrasm}`}
              size="12"
              middle
            >
              Result not found
            </MDBCol>
          )}
        </MDBRow>
      </MDBContainer>
    </>
  );
}

export { Dashboard };
export default Dashboard;
