import React from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBNavbar,
  MDBNavbarBrand,
} from "mdbreact";
import { useRouter } from "next/router";
import style from "./header.module.scss";

function Header() {
  const router = useRouter();

  const goToHome = () => {
    router.push({
      pathname: `/home/`,
    });
  };
  var minWidthValue = "992px";
  if (router.pathname.includes("detail")) {
    minWidthValue = "1635px";
  }
  return (
    <MDBContainer
      style={{
        paddingLeft: "0px",
        paddingRight: "0px",
        minWidth: minWidthValue,
      }}
      className={`w-100 mw-100 ${style.containercustomsm}`}
    >
      <header>
        <MDBNavbar
          style={{
            textAlign: "center",
            height: "80px",
            backgroundColor: "#f1f1f1",
            boxShadow:
              "0 5px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%)",
          }}
          dark
          expand="md"
        >
          <MDBRow className="w-100">
            <MDBCol size="2">
              <MDBRow>
                <MDBCol>
                  <MDBNavbarBrand
                    style={{
                      fontSize: "1.5rem",
                      marginLeft: "10px",
                      cursor: "pointer",
                    }}
                    onClick={goToHome}
                  >
                    <strong className="black-text">
                      Covid Tracker - India
                    </strong>
                  </MDBNavbarBrand>
                </MDBCol>
              </MDBRow>
            </MDBCol>

            <MDBCol size="8">
              <a
                style={{ fontSize: "1.5rem" }}
                className="black-text font-weight-bold"
                onClick={goToHome}
              >
                Zoho
              </a>
            </MDBCol>
          </MDBRow>
        </MDBNavbar>
      </header>
    </MDBContainer>
  );
}
export { Header };
export default Header;
