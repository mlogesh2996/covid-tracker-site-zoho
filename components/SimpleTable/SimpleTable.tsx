import React from "react";
import { ITableProps } from "./typings";
import { useTable } from "react-table";
function SimpleTable(props: ITableProps) {
  const { data, columns } = props;
  const {
    getTableProps, // table props from react-table
    getTableBodyProps, // table body props from react-table
    headerGroups, // headerGroups, if your table has groupings
    rows, // rows for the table based on the data passed
    prepareRow, // Prepare the row (this function needs to be called for each row before getting the row props)
  } = useTable({
    columns,
    data,
  });

  /* 
    Render the UI for your table
    - react-table doesn't have UI, it's headless. We just need to put the react-table props from the Hooks, and it will do its magic automatically
  */
  return (
    <div style={{ textAlign: "center", marginLeft: "3rem" }}>
      {data.length === 0 ? (
        <div>Result not found!</div>
      ) : (
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup: any, indx: number) => (
              <tr key={indx} {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column: any, ind: number) => (
                  <th key={ind} {...column.getHeaderProps()}>
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row: any, i: any) => {
              prepareRow(row);
              return (
                <tr key={i} {...row.getRowProps()}>
                  {row.cells.map((cell: any, x: number) => {
                    return (
                      <td key={x} {...cell.getCellProps()}>
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
}
export { SimpleTable };
export default SimpleTable;
