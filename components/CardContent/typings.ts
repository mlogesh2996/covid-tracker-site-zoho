export interface IFilterContainerProps {
  currentFilterType?: string;
  filterValues?: string;
  response?: any;
  timeSeriesResponse?: any;
  stateData?: any;
  stateCode?: any;
  setFilterValues?: (data: any) => void;
  setCurrentFilterType?: (data: any) => void;
}
