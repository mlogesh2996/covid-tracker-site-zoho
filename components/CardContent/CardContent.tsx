import React, { useState } from "react";
import style from "./cardcontent.module.scss";
import { MDBRow, MDBCol, MDBIcon } from "mdbreact";
import { useRouter } from "next/router";
import { IFilterContainerProps } from "./typings";

function CardContent(props: IFilterContainerProps) {
  const router = useRouter();
  const { stateData, stateCode } = props;

  const [selectedDistrict, setSelectedDistrict]: any = useState();
  const [currentInlinePage, setCurrentInlinePage] = useState(1);

  const goToDetailPage = (stateCode: any) => {
    router.push({
      pathname: `/detail/${stateCode}`,
    });
  };

  const handleDistrictChange = (e: any) => {
    e.preventDefault();
    setSelectedDistrict(e.target.value);
  };

  let confirmed, recovered, deceased;
  let typeToDisplay = "total";
  if (
    selectedDistrict &&
    selectedDistrict !== "Choose district" &&
    stateData[stateCode]?.districts
  ) {
    switch (currentInlinePage) {
      case 1:
        typeToDisplay = "total";
        break;
      case 2:
        typeToDisplay = "delta";
        break;
      case 3:
        typeToDisplay = "delta7";
        break;
    }
    confirmed =
      stateData[stateCode]?.districts[selectedDistrict]?.[typeToDisplay]
        ?.confirmed || 0;
    recovered =
      stateData[stateCode]?.districts[selectedDistrict]?.[typeToDisplay]
        ?.recovered || 0;
    deceased =
      stateData[stateCode]?.districts[selectedDistrict]?.[typeToDisplay]
        ?.deceased || 0;
  } else {
    switch (currentInlinePage) {
      case 1:
        typeToDisplay = "total";
        break;
      case 2:
        typeToDisplay = "delta";
        break;
      case 3:
        typeToDisplay = "delta7";
        break;
    }
    confirmed = stateData[stateCode]?.[typeToDisplay]?.confirmed || 0;
    recovered = stateData[stateCode]?.[typeToDisplay]?.recovered || 0;
    deceased = stateData[stateCode]?.[typeToDisplay]?.deceased || 0;
  }
  return (
    <div
      className={`card ${style.customcard}`}
      // onClick={() => goToDetailPage(stateCode)}
    >
      <div style={{ height: "270px" }} className="card-body">
        <MDBRow
          style={{ textAlign: "center", pointerEvent: "none" }}
          className="w-100"
        >
          <MDBCol size="6">
            <h3
              style={{
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: "16px",
                textAlign: "left",
              }}
              className={`ml-4 card-title ${style.covidTitle} mb-1`}
            >
              {stateCode}
            </h3>
          </MDBCol>
          <MDBCol size="6">
            <select
              name="sort"
              className="browser-default custom-select"
              onChange={handleDistrictChange}
            >
              <option value={"Choose district"}>Choose district</option>;
              {stateData[stateCode]?.districts &&
                Object.keys(stateData[stateCode]?.districts).length > 0 &&
                Object.keys(stateData[stateCode]?.districts).map(
                  (district, i) => {
                    return (
                      <option key={i} value={district}>
                        {district}
                      </option>
                    );
                  }
                )}
            </select>
          </MDBCol>
        </MDBRow>
        <MDBRow
          style={{ textAlign: "center", pointerEvent: "none" }}
          className="w-100 ml-0"
        >
          <MDBCol
            size="2"
            style={{ alignSelf: "center" }}
            onClick={() => {
              if (currentInlinePage > 1) {
                setCurrentInlinePage((prevActiveStep) => prevActiveStep - 1);
              }
            }}
          >
            {currentInlinePage !== 1 && <MDBIcon icon="angle-left" />}
          </MDBCol>
          <MDBCol size="8">
            <div onClick={() => goToDetailPage(stateCode)}>
              <p
                style={{
                  fontSize: "15px",
                  fontWeight: 500,
                  color: "#28a745",
                  fontStyle: "initial",
                  textTransform: "capitalize",
                }}
                className={`mt-4 mb-2 card-text  ${style.covidTitle} mb-1`}
              >
                {typeToDisplay}
              </p>
              {stateData[stateCode]?.total?.confirmed && (
                <div
                  style={{
                    fontSize: "0.90rem",
                    fontStyle: "normal",
                    fontWeight: 500,
                    color: "#6d6d6d",
                  }}
                  className="mt-1"
                >
                  {"Confirmed: "}
                  {confirmed}
                </div>
              )}
              {stateData[stateCode]?.total?.recovered && (
                <div
                  style={{
                    fontSize: "0.90rem",
                    fontStyle: "normal",
                    fontWeight: 500,
                    color: "#6d6d6d",
                  }}
                  className="mt-1"
                >
                  {"Recovered: "}
                  {recovered}
                </div>
              )}
              {stateData[stateCode]?.total?.deceased && (
                <div
                  style={{
                    fontSize: "0.90rem",
                    fontStyle: "normal",
                    fontWeight: 500,
                    color: "#6d6d6d",
                  }}
                  className="mt-1"
                >
                  {"Deceased: "}
                  {deceased}
                </div>
              )}
            </div>
          </MDBCol>
          <MDBCol
            size="2"
            style={{ alignSelf: "center" }}
            onClick={() => {
              if (currentInlinePage < 3) {
                setCurrentInlinePage((prevActiveStep) => prevActiveStep + 1);
              }
            }}
          >
            {currentInlinePage !== 3 && <MDBIcon icon="angle-right" />}
          </MDBCol>
        </MDBRow>
      </div>
    </div>
  );
}

export { CardContent };
export default CardContent;
