function getSearchResult(searchTerm: string, data: any) {
  let filteredStateCode: any = [];
  if (searchTerm) {
    filteredStateCode = Object.keys(data).filter((val: any) => {
      return val.toLowerCase().includes(searchTerm.toLocaleLowerCase());
    });
  }
  return filteredStateCode;
}

function getSortedResult(type: any, data: any) {
  let sortedStateCodes: any = [];
  if (type === "0") {
    //No Choice
    sortedStateCodes = Object.keys(data);
  } else if (type === "1") {
    //Confirmed ASC
    sortedStateCodes = Object.keys(data).sort(
      (a, b) => data[a].total.confirmed - data[b].total.confirmed
    );
  } else if (type === "2") {
    //Affected Percentage ASC
    sortedStateCodes = Object.keys(data).sort(
      (a, b) =>
        (data[a].total.confirmed / data[a].total.tested) * 100 -
        (data[b].total.confirmed / data[b].total.tested) * 100
    );
  } else if (type === "3") {
    //Vaccinated Percentage ASC
    sortedStateCodes = Object.keys(data).sort(
      (a, b) =>
        (data[a].total.vaccinated1 / data[a].meta.population) * 100 -
        (data[b].total.vaccinated1 / data[b].total.population) * 100
    );
  } else if (type === "4") {
    //Confirmed DESC
    sortedStateCodes = Object.keys(data).sort(
      (a, b) => data[b].total.confirmed - data[a].total.confirmed
    );
  } else if (type === "5") {
    //Affected Percentage DESC
    sortedStateCodes = Object.keys(data).sort(
      (a, b) =>
        (data[b].total.confirmed / data[b].total.tested) * 100 -
        (data[a].total.confirmed / data[a].total.tested) * 100
    );
  } else if (type === "6") {
    //Vaccinated Percentage DESC
    sortedStateCodes = Object.keys(data).sort(
      (a, b) =>
        (data[b].total.vaccinated1 / data[b].meta.population) * 100 -
        (data[a].total.vaccinated1 / data[a].meta.population) * 100
    );
  }
  return sortedStateCodes;
}

function getSortedResultforTable(type: any, data: any) {
  let result;
  if (type === "0") {
    //No Choice
    result = data;
  } else if (type === "1") {
    //Confirmed ASC
    result = data.sort((a: any, b: any) => a.confirmed - b.confirmed);
  } else if (type === "2") {
    //Confirmed DESC
    result = data.sort((a: any, b: any) => b.confirmed - a.confirmed);
  } else if (type === "3") {
    //Recovered ASC
    result = data.sort((a: any, b: any) => a.recovered - b.recovered);
  } else if (type === "4") {
    //Recovered DESC
    result = data.sort((a: any, b: any) => b.recovered - a.recovered);
  } else if (type === "5") {
    //Affected ASC
    result = data.sort((a: any, b: any) => a.deceased - b.deceased);
  } else if (type === "6") {
    //Vaccinated DESC
    result = data.sort((a: any, b: any) => b.deceased - a.deceased);
  }
  return result;
}

export { getSearchResult, getSortedResult, getSortedResultforTable };
